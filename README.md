# manjaro-kernel

## GRUB  


````
set timeout=25

menuentry 'Linux manjaro kernel 6.1 from sda1' --class devuan --class gnu-linux --class gnu { 
	insmod gzio
	insmod part_msdos
	insmod ext2
	set root='hd0,msdos1'
	linux	/boot/vmlinuz-6.1-x86_64 root=/dev/sda1  rw 
	initrd	/boot/initramfs-6.1-x86_64.img
}
````


